# -*-coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from Register.views import register, logout, login, redir


urlpatterns = patterns('',
                       url(r'register/', register),
                       url(r'login/', login),
                       url(r'logout/', logout),
                       url(r'notes/', include('Notes_entity.urls')),
                       url(r'', redir),
                      )