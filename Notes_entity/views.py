# -*-coding: utf-8 -*-
from annoying.decorators import ajax_request
from django.http import HttpResponse
from django.shortcuts import redirect, render_to_response
from Notes_entity.models import Notes, Category
from django.template import RequestContext, loader
from django.core.context_processors import csrf
from Notes_entity.forms import Note
from django.template.defaultfilters import date as _date

def all_notes(request, username):
    if username != request.user.username:
        return redirect('/')
    args = {}
    args.update(csrf(request))
    args['data'] = []
    farm = Note
    args['form'] = farm
    for i in Notes.objects.all().filter(note_user=request.user):
        e = 0
        if i.uuid_boolean:
            print(request)
            e = "http://127.0.0.1:8888" + i.get_absolute_url()
        args['data'].insert(0, ([i.id,
                                 i.note_header,
                                 i.note_time,
                                 i.note_category,
                                 i.note_text,
                                 i.note_favorites,
                                 e]))
    args['category_s'] = Category.objects.values().filter(category_user=1)
    if Category.objects.values().filter(category_user=request.user):
        args['category_m'] = Category.objects.values().filter(category_user=request.user)
    else:
        args['category_m'] = False
    t = loader.get_template('notes.html')
    c = RequestContext(request, args)
    return HttpResponse(t.render(c))

@ajax_request
def del_note(request, username, id):
    if not request.user.username == username:
        return 'error'
    else:
        try:
            Notes.objects.get(pk=id).delete()
            return 1
        except Notes.DoesNotExist:
            return 'error'

@ajax_request
def create_note(request, username, id):
    if not request.user.username == username:
        return 'error'
    else:
        favorites = 1 if request.POST.get('favorites') == "true" else 0
        category = Category.objects.get(pk=request.POST.get('category'))
        try:
            farm = Notes.objects.get(pk=id)
            farm.note_text = request.POST.get('text')
            farm.note_header = request.POST.get('header')
            farm.note_category = category
            farm.note_favorites = favorites
            farm.save()
            return 1
        except Notes.DoesNotExist:
            if request.method == 'POST' and id == "0":
                farm = Notes.objects.create(note_user=request.user,
                                            note_text=request.POST.get('text'),
                                            note_header=request.POST.get('header'),
                                            note_favorites=favorites,
                                            note_category=category
                )
                farm.save()
                time = _date(farm.note_time, "d E Y г. G:i")
                args = {'data': 0, 'pk': farm.pk, 'tim': time}
                return args
            else:
                return 'error'

@ajax_request
def cat_add(request, username):
    if not request.user.username == username or not request.POST.get('category'):
        return 'error'
    else:
        try:
            Category.objects.get(category_text=request.POST.get('category'))
            return {'stat':0}
        except Category.DoesNotExist:
            farm = Category.objects.create(category_user=request.user, category_text=request.POST.get('category'))
            farm.save()
            return {'stat': 1, 'id': Category.objects.latest('id').id}

@ajax_request
def cat_del(request, username):
    if not request.user.username == username or not request.POST.get('category'):
        return 'error'
    else:
        try:
            category = Category.objects.get(category_text=request.POST.get('category'))
            notes = Notes.objects.filter(note_category=category)
            notes.delete()
            id = category.id
            category.delete()
            return {'stat': 1, 'id': id}
        except Category.DoesNotExist:
            return {'stat': 0}

@ajax_request
def sort_note(request, username):
    if not request.user.username == username or not request.POST.get('s_category') \
            or not request.POST.get('s_cret'):
        return 'error'
    else:
        args = []
        if request.POST.get('s_category') == '1':
            m = "" if request.POST.get('s_cret') == '1' else "-"
            m += "note_time"
        elif request.POST.get('s_category') == '2':
            m = "" if request.POST.get('s_cret') == '1' else "-"
            m += "note_category"
        elif request.POST.get('s_category') == '3':
            m = "" if request.POST.get('s_cret') == '1' else "-"
            m += "note_favorites"
        else:
            return 'error'
        farm = Notes.objects.filter(note_user=request.user).order_by(m)
        for i in farm:
            args.append(i.id)
        return args

@ajax_request
def search_note(request, username):
    if not request.user.username == username or not request.POST.get('idi') \
            or not request.POST.get('data'):
        return 'error'
    else:
        args = []
        if request.POST.get('idi') == '1':
            d = request.POST.get('data')[0:2]
            m = request.POST.get('data')[3:5]
            y = request.POST.get('data')[6:]
            date = y + '-' + m + '-' + d
            if len(Notes.objects.filter(note_user=request.user, note_date=date)) > 0:
                farm = Notes.objects.filter(note_user=request.user, note_date=date)
                for i in farm:
                    args.append(i.id)
                return args
            else:
                return 'not'
        elif request.POST.get('idi') == '2':
            print(2)
            if Notes.objects.filter(note_user=request.user, note_header=request.POST.get('data')):
                farm = Notes.objects.filter(note_user=request.user, note_header=request.POST.get('data'))
                for i in farm:
                    args.append(i.id)
                return args
            else:
                return 'not'
        elif request.POST.get('idi') == '3':
            print(3)
            if Notes.objects.filter(note_user=request.user, note_category=request.POST.get('data')):
                farm = Notes.objects.filter(note_user=request.user, note_category=request.POST.get('data'))
                for i in farm:
                    args.append(i.id)
                return args
            else:
                return 'not'
        elif request.POST.get('idi') == '4':
            print(4)
            f = 1 if request.POST.get('data') == "1" else 0
            if Notes.objects.filter(note_user=request.user, note_favorites=f):
                farm = Notes.objects.filter(note_user=request.user, note_favorites=f)
                for i in farm:
                    args.append(i.id)
                return args
            else:
                return 'not'
        else:
            return 'not'

def access_denied(request, uuid):
    try:
        farm = Notes.objects.get(uuid=uuid)
        args = {}
        args.update(csrf(request))
        print(uuid)
        if farm.uuid_boolean:
            args['note'] = farm.note_text
        else:
            args['note'] = "Доступ закрыт."
    except Notes.DoesNotExist:
        args['note'] = "Неверный Url"
    c = RequestContext(request, args)
    return render_to_response('note.html', c)