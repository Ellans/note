# -*-coding: utf-8 -*-
from django.conf.urls import patterns, url
from Notes_entity.views import all_notes, del_note, create_note, cat_add, cat_del, sort_note, search_note, \
    access_denied
from Register.views import redir

urlpatterns = patterns('',
                        url(r'(?P<username>\w+)/del_(?P<id>\d+)/', del_note),
                        url(r'(?P<username>\w+)/new_(?P<id>\d+)/', create_note),
                        url(r'(?P<username>\w+)/sorted/', sort_note),
                        url(r'(?P<username>\w+)/search/', search_note),
                        url(r'note/(?P<uuid>\w+)/', access_denied),
                        url(r'(?P<username>\w+)/', all_notes),
                        url(r'(?P<username>\w+)/category_plus/', cat_add),
                        url(r'(?P<username>\w+)/category_del/', cat_del),
                        url(r'', redir),
                      )