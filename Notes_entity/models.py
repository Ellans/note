# -*-coding: utf-8 -*-
import datetime
from django.db import models
from django.contrib.auth.models import User
from uuidfield import UUIDField


class Category(models.Model):

    class Meta():
        db_table = 'category'

    category_text = models.CharField(max_length=250)
    category_user = models.ForeignKey(User)

    def __unicode__(self):
        return self.category_text


class Notes(models.Model):

    class Meta():

        db_table = 'notes'

    uuid = UUIDField(auto=True)
    uuid_boolean = models.BooleanField(default=False)
    note_header = models.CharField(max_length=250)
    note_time = models.DateTimeField(auto_now_add=True, blank=True)
    note_date = models.DateField(default=datetime.date.today())
    note_text = models.TextField(("Заметка"), blank=True, null=True)
    note_favorites = models.BooleanField(default=False)
    note_user = models.ForeignKey(User)
    note_category = models.ForeignKey(Category)

    def get_absolute_url(self):
        return '/notes/note/{}/'.format(self.uuid)
