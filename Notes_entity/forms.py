# -*-coding: utf-8 -*-
from django.forms import ModelForm
from Notes_entity.models import Notes


class Note(ModelForm):

    class Meta():
        model = Notes
        fields = ['note_text']

    class Media:
        js = ('/static/js/tiny_mce/tiny_mce.js',
              '/static/js/tiny_mce/textareas.js',
            )