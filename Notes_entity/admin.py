# -*-coding: utf-8 -*-
from django.contrib import admin
from Notes_entity.models import Notes, Category



class Art(admin.ModelAdmin):

    class Media:
        js = ('/static/js/tiny_mce/tiny_mce.js',
              '/static/js/tiny_mce/textareas.js'
            )

admin.site.register(Notes, Art)
admin.site.register(Category)
# Register your models here.
