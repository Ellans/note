from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^auth/', include('Register.urls')),
    url(r'^notes/', include('Notes_entity.urls')),
    url(r'^', include('Register.urls')),
)
